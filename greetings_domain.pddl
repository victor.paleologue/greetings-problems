(define (domain greetings_domain)
  (:requirements :adl :negative-preconditions :universal-preconditions)

  ; We use the DUL core ontology, found at:
  ; http://www.ontologydesignpatterns.org/ont/dul/DUL.owl
  (:types
    ; DUL
    natural_person - physical_agent
    social_person - social_agent
    physical_agent social_agent - agent
    physical_agent - physical_object
    role - concept
    concept - social_object
    physical_object social_object agent - object
    ; epistemic
    predicate - object

    ; custom

    )

  (:constants
    ; DUL
    self - natural_person
    pragmatic_slot - role
    ; epistemic of DUL
    ; epistemic of custom
    looks_at engages can_engage was_greeted identified colocated answer_is_42
    ; epistemic of epistemic
    believes_0 believes_1 believes_2 - predicate
    ; custom
    placeholder - object ; something yet to be (open world)
    )

  (:predicates
    ; DUL
    (has_role ?o - object ?r - role)

    ; epistemic
    (believes_0 ?a - agent ?p - predicate)
    (believes_1 ?a - agent ?p - predicate ?o1 - object)
    (believes_2 ?a - agent ?p - predicate ?o1 - object ?o2 - object)
    (believes_3 ?a - agent ?p - predicate ?o1 - object ?o2 - object ?o3 - object)

    ; custom
    (looks_at ?looker - natural_person ?looked - natural_person)
    (engages ?a1 - natural_person ?a2 - natural_person)
    (can_engage ?a1 - natural_person ?a2 - natural_person)
    (was_greeted ?h - natural_person)
    (identified ?identifier - agent ?identified - agent)
    (colocated ?a1 - physical_agent ?a2 - physical_agent)
    (answer_is_42)
  )

  ; Greeting in body requires a visual or auditive proximity.
  ; Technically, we greet a physical embodiment.
  ; A priori, we only deal with humans: "natural persons".
  ; For better success, it is recommended that self and ?h engage each other first.
  (:action greet_anonymously
    :parameters (?h - natural_person)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (was_greeted ?h)
  )

  ; Known persons are greeted physically too,
  ; but they are associated to a known agent.
  ; Intuitively that would be a social person,
  ; but it is not a requirement.
  (:action greet_known_person
    :parameters (?h - natural_person ?s - agent)
    :precondition (and (engages self ?h) (colocated self ?h) (identified self ?h))
    ; Greeted agent knows that the greeter recognized them.
    :effect (and
      (was_greeted ?h)
      (believes_2 ?s identified self ?s)
    )
  )

  (:action tell_statement_0
    :parameters (?h - natural_person ?p - predicate)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (believes_0 ?h ?p)
  )

  (:action tell_statement_1
    :parameters (?h - natural_person ?p - predicate ?o1 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (believes_1 ?h ?p ?o1)
  )

  (:action tell_statement_2
    :parameters (?h - natural_person ?p - predicate ?o1 - object ?o2 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (believes_2 ?h ?p ?o1 ?o2)
  )

  (:action tell_statement_3
    :parameters (?h - natural_person ?p - predicate ?o1 - object ?o2 - object ?o3 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (believes_3 ?h ?p ?o1 ?o2 ?o3)
  )

  (:action tell_any
    :parameters (?h - natural_person ?o - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (has_role ?o pragmatic_slot))
  )

  (:action ask_statement_0
    :parameters (?h - natural_person ?p - predicate)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_0 self ?p) (believes_0 ?h ?p))
  )

  (:action ask_statement_1
    :parameters (?h - natural_person ?p - predicate ?o1 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_1 self ?p ?o1) (believes_1 ?h ?p ?o1))
  )

  (:action ask_statement_2
    :parameters (?h - natural_person ?p - predicate ?o1 - object ?o2 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_2 self ?p ?o1 ?o2) (believes_2 ?h ?p ?o1 ?o2))
  )

  (:action ask_statement_3
    :parameters (?h - natural_person ?p - predicate ?o1 - object ?o2 - object ?o3 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_3 self ?p ?o1 ?o2 ?o3) (believes_3 ?h ?p ?o1 ?o2 ?o3))
  )

  (:action ask_0_arg_0
    :parameters (?h - natural_person)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_0 self placeholder) (believes_0 ?h placeholder))
  )

  (:action ask_0_arg_1
    :parameters (?h - natural_person ?o1 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_1 self placeholder ?o1) (believes_1 ?h placeholder ?o1))
  )

  (:action ask_1_arg_1
    :parameters (?h - natural_person ?p - predicate)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_1 self ?p placeholder) (believes_1 ?h ?p placeholder))
  )

  (:action ask_0_arg_2
    :parameters (?h - natural_person ?o1 - object ?o2 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_2 self placeholder ?o1 ?o2) (believes_2 ?h placeholder ?o1 ?o2))
  )

  (:action ask_1_arg_2
    :parameters (?h - natural_person ?p - predicate ?o2 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_2 self ?p placeholder ?o2) (believes_2 ?h ?p placeholder ?o2))
  )

  (:action ask_2_arg_2
    :parameters (?h - natural_person ?p - predicate ?o1 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_2 self ?p ?o1 placeholder) (believes_2 ?h ?p ?o1 placeholder))
  )

  (:action ask_0_arg_3
    :parameters (?h - natural_person ?o1 - object ?o2 - object ?o3 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_3 self placeholder ?o1 ?o2 ?o3) (believes_3 ?h placeholder ?o1 ?o2 ?o3))
  )

  (:action ask_1_arg_3
    :parameters (?h - natural_person ?p - predicate ?o2 - object ?o3 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_3 self ?p placeholder ?o2 ?o3) (believes_3 ?h ?p placeholder ?o2 ?o3))
  )

  (:action ask_2_arg_3
    :parameters (?h - natural_person ?p - predicate ?o1 - object ?o3 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_3 self ?p ?o1 placeholder ?o3) (believes_3 ?h ?p ?o1 placeholder ?o3))
  )

  (:action ask_3_arg_3
    :parameters (?h - natural_person ?p - predicate ?o1 - object ?o2 - object ?o3 - object)
    :precondition (and (engages self ?h) (colocated self ?h))
    :effect (and (believes_3 self ?p ?o1 ?o2 placeholder) (believes_3 ?h ?p ?o1 ?o2 placeholder))
  )

  (:action start_engage
    :parameters (?h - natural_person)
    :precondition (and (can_engage self ?h))
    :effect (engages self ?h)
  )

  (:action stop_engage
    :parameters (?h - natural_person)
    :precondition (engages self ?h)
    :effect (not (engages self ?h))
  )
)