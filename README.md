# Greetings Problems for Social Robots

PDDL problems for a robot greeting and exchanging information with a human.

[`greetings_domain.pddl`](greetings_domain.pddl) is the shared domain file used by all the problems,
named `greetings_*.pddl`.
Each problem is associated to an expected plan, `greetings_*.plan`.

[`greetings_domain.ptest.json`](greetings_domain.ptest.json) lists each problem and its expected plan,
so that to be tested automatically using the PDDL extension for VSCode.
