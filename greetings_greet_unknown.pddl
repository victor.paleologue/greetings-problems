(define (problem sandbox_problem)
  (:domain greetings_domain)
  (:requirements :adl :negative-preconditions :universal-preconditions)

  (:objects
    human_93333 - natural_person ; there is a human
    )

  (:init
    (colocated self human_93333) ; human is around
    (believes_2 self colocated self human_93333) ; I am aware the human is around
    (can_engage self human_93333) ; human can be engaged
    (engages human_93333 self) ; human engages us
  )

  (:goal
    (and
      ; everyone that...
      (forall
        (?h - natural_person)
        (and
          ; ... can be engaged should be engaged
          (imply
            (can_engage self ?h)
            (engages self ?h))
          ; ... is engaged should be greeted
          (imply
            (engages self ?h)
            (and
              (was_greeted ?h)
            )
          )
        )
      )
    )
  )
)