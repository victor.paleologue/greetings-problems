(define (problem sandbox_problem)
  (:domain greetings_domain)
  (:requirements :adl :negative-preconditions :universal-preconditions)

  (:objects
    human_93333 - natural_person ; there is a human
    )

  (:init
    (colocated self human_93333) ; human is around
    (believes_2 self colocated self human_93333) ; I am aware the human is around
    (believes_2 self identified self human_93333) ; I am aware I identified the human
    (can_engage self human_93333) ; human can be engaged
    (engages human_93333 self) ; human engages us
    (identified self human_93333) ; human is identified
  )

  (:goal
    (and
      (forall
        (?h - natural_person)
        (and
          (imply
            (engages self ?h)
            (and
              (was_greeted ?h)
            )
          )
          (imply
            (identified self ?h)
            (believes_2 ?h identified self ?h)
          )
        )
      )
    )
  )
)